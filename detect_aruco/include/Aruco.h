#include<opencv2/aruco.hpp>
#include<opencv2/opencv.hpp>
#include<vector>

using namespace std;

class Aruco{
    public:
        Aruco(); //constructeur
        ~Aruco(); //destructeur
        cv::Mat detectAruco(cv::Mat image); // detect la presence de code aruco sur limage donnee
        void makeAruco();
        void makeArucoBoard();
        vector<int> getIdentifiants();
        vector<vector<cv::Point2f>> getAllPositions();
        vector<cv::Point2f> getPositionCentre();


    private:
        vector<int> identifiant_;
        vector<vector<cv::Point2f>> allPostions_;
        vector<cv::Point2f> positionCentre_;
};