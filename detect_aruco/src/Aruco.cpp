/*
Auteur: Guillaume Girouard
Date: Fevrier 2022
Description: Module de detection de code Aruco
*/

#include"../include/Aruco.h"

using namespace std;
using namespace cv;

Aruco::Aruco(){}
Aruco::~Aruco(){}
Mat Aruco::detectAruco(Mat image){
    //detect Aruco from image
    Mat imageReturn;
    image.copyTo(imageReturn);
    Ptr<aruco::Dictionary> reference = aruco::getPredefinedDictionary(aruco::DICT_6X6_250);
    vector<int> identifications;
    vector<vector<Point2f>> coins;
    aruco::detectMarkers(image,reference,coins,identifications);
    if(identifications.size() >= 1){
        aruco::drawDetectedMarkers(imageReturn, coins, identifications);
        identifiant_ = identifications;
        allPostions_ = coins;
    }
    imwrite("../image/resultat_detections.png",imageReturn);
    return imageReturn;
}
void Aruco::makeAruco(){
    Mat ArucoImage;
    Ptr<aruco::Dictionary> reference = aruco::getPredefinedDictionary(aruco::DICT_6X6_250);
    aruco::drawMarker(reference, 23, 200, ArucoImage, 1);
    imwrite("../image/marker.png",ArucoImage);
}
void Aruco::makeArucoBoard(){
    Ptr<aruco::Dictionary> reference = aruco::getPredefinedDictionary(aruco::DICT_6X6_250);
    Ptr<aruco::GridBoard> board = aruco::GridBoard::create(5, 7, 0.04, 0.01, reference);
    Mat boardImage;
    board->draw(Size(600, 500), boardImage, 10, 1);
    imwrite("../image/boardMarker.png",boardImage);
}
vector<int> Aruco::getIdentifiants(){
    return identifiant_;
}

vector<vector<Point2f>> Aruco::getAllPositions(){
    return allPostions_;
}
vector<Point2f> Aruco::getPositionCentre(){
    positionCentre_.clear();
    for(int i =0; i<allPostions_.size();i++){
        Point2f temp;
        temp.x = round((allPostions_[i][0].x+allPostions_[i][2].x)/2);
        temp.y = round((allPostions_[i][0].y+allPostions_[i][2].y)/2);
        positionCentre_.push_back(temp);
    }
    return positionCentre_;
}