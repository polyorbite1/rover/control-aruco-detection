/**************************************
**Auteur: Guillaume Girouard
**Compagnie: Polyorbite
**Date: Fevrier 2022
**Description: demonstration de fonctionnement de la lecture de code Aruco
**Le code suivant prend une image Mat Opencv dune camera et fait la reocnnaissance de code aruco filmee
**************************************/

#include"../include/Aruco.h"

using namespace std;

int main(){
    Aruco aruco;
    //aruco.makeAruco();
    //aruco.makeArucoBoard();
    cv::VideoCapture cap = cv::VideoCapture(0);
    cv::Mat image; //cv::imread("../image/boardMarker.png");
    for(;;){
        cap.read(image);
        cv::Mat imageResult = aruco.detectAruco(image);
        vector<int> ids = aruco.getIdentifiants();
        vector<vector<cv::Point2f>> pos = aruco.getAllPositions();
        vector<cv::Point2f> posC = aruco.getPositionCentre();
        for( int i = 0; i<ids.size(); i++){
            cout << "identifications: " << endl;
            cout << ids[i] << endl;
            cout << "positions: " << endl;
            cout << pos[i] << endl;
            cout << "positions Centre: " << endl;
            cout << posC[i] << endl;
        }
        cv::imshow("...",imageResult);
        if(cv::waitKey(20) == 27){
            break;
        }
    }
    cap.release();
    cv::destroyAllWindows();
    return 0;
}
